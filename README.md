# termsize

`termsize` is a simple utility returning the size of the terminal in Linux.

## Usage

The `termsize` command will return the number of columns and lines the terminal uses.  To return just the number of columns, use `termsize x`.  For just the number of lines, use `termsize y`.

## Compiling

`termsize` can be compiled with practically any C compiler on Linux, but for `gcc`, use `gcc termsize.c -o termsize; chmod +x termsize`.

## Licence

`termsize` is MIT licenced.

## Author

DHeadshot.  I'm on the fediverse at @dheadshot@mastodon.social if that helps.

