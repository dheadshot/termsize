#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>

int main(int argc, char *argv[])
{
  struct winsize w;
  ioctl(0,TIOCGWINSZ, &w);
  if (argc<2 || strcmp(argv[1],"xy")==0) printf("%d,%d\n",w.ws_col,w.ws_row);
  else if (strcmp(argv[1],"x")==0) printf("%d\n",w.ws_col);
  else if (strcmp(argv[1],"y")==0) printf("%d\n",w.ws_row);
  else return 1;
  return 0;
}
